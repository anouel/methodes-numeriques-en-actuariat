%%% Copyright (C) 2018 Vincent Goulet
%%%
%%% Ce fichier fait partie du projet
%%% «Méthodes numériques en actuariat avec R»
%%% https://gitlab.com/vigou3/methodes-numeriques-en-actuariat
%%%
%%% Cette création est mise à disposition sous licence
%%% Attribution-Partage dans les mêmes conditions 4.0
%%% International de Creative Commons.
%%% https://creativecommons.org/licenses/by-sa/4.0/

\chapter*{Introduction}
\addcontentsline{toc}{chapter}{Introduction}
\markboth{Introduction}{Introduction}

L'algèbre linéaire a le chic de surgir là où on ne l'attend pas
nécessairement. On croit qu'en étudiant l'actuariat, les notions de
vecteurs et de matrices nous demeurerons étrangères. Et pourtant.

Les matrices et leur algèbre permettent de représenter, de traiter et
de résoudre efficacement de grands systèmes d'équations linéaires ou
d'équations différentielles. La notion d'erreur quadratique moyenne
s'apparente à la projection d'un vecteur dans un espace vectoriel. Les
notions d'indépendance stochastique et d'orthogonalité de vecteurs
sont liées. On décrit le comportement d'une chaîne de Markov à l'aide
d'une matrice de transition. Une classe de lois de probabilités
requiert de calculer l'exponentielle d'une matrice. Ce ne sont là que
quelques exemples où l'algèbre linéaire joue un rôle en théorie des
probabilités, en inférence statistique, en finance ou en théorie du
risque.

Le \autoref{chap:notions} revient sur les principales notions
d'algèbre linéaire normalement étudiées au collège et qu'il est
important de maîtriser dans ses études de premier cycle universitaire.
Nous nous concentrons sur l'établissement de liens entre des éléments
qui peuvent au premier chef sembler disparates. Le
\autoref{chap:valeurs_propres} construit sur le précédent pour
introduire les concepts de valeurs propres, de vecteurs propres et de
diagonalisation d'une matrice. Ceux-ci jouent un rôle, entre autres,
en finance mathématique. Pour terminer sur des considérations
numériques ayant jusque là traversé le cours, le
\autoref{chap:decomposition} expose et compare très succinctement
différentes stratégies utilisées pour résoudre des systèmes
d'équations linéaires à l'aide d'un ordinateur.

Cette troisième partie de l'ouvrage diffère passablement des deux
autres, autant dans sa nature que dans le format des activités
d'apprentissage. La matière y est beaucoup plus mathématique et
abstraite, il y a de nombreux exercices et les considérations
informatiques sont réduites au minimum. Les
chapitres~\ref{chap:notions} et \ref{chap:valeurs_propres} ne
comportent que de courtes sections faisant la démonstration de
fonctions R dédiées à l'algèbre linéaire. Seul le chapitre
\autoref{chap:valeurs_propres} propose un problème à résoudre au fil du
texte.

\section*{Fonctionnalités interactives}

En consultation électronique, ce document se trouve enrichi de
plusieurs fonctionnalités interactives.
\begin{itemize}
\item Intraliens du texte vers une ligne précise d'une section de code
  informatique et, en sens inverse, du numéro de la ligne vers le
  point de la référence dans le texte. Ces intraliens sont marqués par
  la couleur \textcolor{link}{\rule{1.5em}{1.2ex}}.
\item Intraliens entre le numéro d'un exercice et sa solution, et vice
  versa. Ces intraliens sont aussi marqués par la couleur
  \textcolor{link}{\rule{1.5em}{1.2ex}}.
\item Intraliens entre les citations dans le texte et leur entrée dans
  la bibliographie. Ces intraliens sont marqués par la couleur
  \textcolor{citation}{\rule{1.5em}{1.2ex}}.
\item Hyperliens vers des ressources externes marqués par le symbole
  {\smaller\faExternalLink} et la couleur
  \textcolor{url}{\rule{1.5em}{1.2ex}}.
\item Table des matières, liste des tableaux, liste des figures et
  liste des vidéos permettant d'accéder rapidement à des ressources du
  document.
\end{itemize}

\section*{Blocs signalétiques}

Le document est parsemé de divers types de blocs signalétiques
inspirés de
\link{https://asciidoctor.org/docs/user-manual/\#admonition}{AsciiDoc}
qui visent à attirer votre attention sur une notion. Vous pourrez
rencontrer l'un ou l'autre des blocs suivants.

\tipbox{Astuce! Ces blocs contiennent un truc, une astuce, ou tout
  autre type d'information utile.}
\vspace{-\baselineskip}

\warningbox{Avertissement! Ces blocs mettent l'accent sur une notion
  ou fournissent une information importante pour la suite.}
\vspace{-\baselineskip}

\cautionbox{Attention! Vous risquez de vous brûler ---
  métaphoriquement s'entend --- si vous ne suivez pas les
  recommandations de ces blocs.}
\vspace{-\baselineskip}

\importantbox{Important! Ces blocs contiennent les remarques les plus
  importantes. Veillez à en tenir compte.}
\vspace{-\baselineskip}

\notebox{Ces blocs contiennent des remarques additionnelles sur
  la matière ou des informations amusantes, mais non essentielles.}
\vspace{-\baselineskip}

%% Utilisation de la commande \awesomebox pour illustrer les blocs de
%% vidéos pour éviter de créer une entrée dans la liste des vidéos.
\awesomebox{\faYoutubePlay}{\aweboxrulewidth}{url}{Ces blocs
  contiennent des liens vers des vidéos dans ma %
  \link{https://www.youtube.com/channel/UCRNwk6mEdUc0njqytxXBDOw/videos}{chaine
    YouTube} dédiée à ce document de référence. Les vidéos sont
  répertoriées dans la liste des vidéos.}
\vspace{-\baselineskip}

\gotorbox{Ces blocs vous invitent à interrompre la lecture du texte
  pour passer à l'étude du code R des sections d'exemples.}
\vspace{-\baselineskip}

\osxbox{Remarques spécifiques à macOS.}

\section*{Document libre}

Tout comme R et l'ensemble des outils présentés dans ce document, le
projet «Méthodes numériques en actuariat avec R» s'inscrit dans le
mouvement de
l'\link{https://www.gnu.org/philosophy/free-sw.html}{informatique
  libre}. Vous pouvez accéder à l'ensemble du code source en format
{\LaTeX} en suivant le lien dans la page de copyright. Vous trouverez
dans le fichier \code{README.md} toutes les informations utiles pour
composer le document.

Votre contribution à l'amélioration du document est également la
bienvenue; consultez le fichier \code{CONTRIBUTING.md} fourni avec ce
document et voyez votre nom ajouté au fichier \code{COLLABORATEURS}.

\section*{Remerciements}

Je tiens à souligner la précieuse collaboration de MM.~Mathieu
Boudreault, de Sébastien Auclair et de Louis-Philippe Pouliot lors de la
rédaction des exercices et des solutions.

%%% Local Variables:
%%% TeX-master: "methodes-numeriques-en-actuariat_algebre-lineaire.tex"
%%% TeX-engine: xetex
%%% coding: utf-8
%%% End:
