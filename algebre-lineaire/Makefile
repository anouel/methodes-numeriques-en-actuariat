### -*-Makefile-*- pour préparer la partie "Simulation stochastique"
###                de "Méthodes numériques en actuariat"
##
## Copyright (C) 2019 Vincent Goulet
##
## 'make pdf' ou 'make all' crée les fichiers .tex à partir des
## fichiers .Rnw avec Sweave et compile le document maitre avec
## XeLaTeX.
##
## 'make tex' crée les fichiers .tex à partir des fichiers .Rnw avec
## Sweave.
##
## 'make Rout' crée les fichiers .Rout avec R CMD BATCH.
##
## 'make install' copie les fichiers pour la préparation de l'archive
## d'une version du projet.
##
## 'make check-url' vérifie la validité de toutes les url présentes
## dans les sources du document.
##
## Auteur: Vincent Goulet
##
## Ce fichier fait partie du projet
## "Méthodes numériques en actuariat avec R"
## https://gitlab.com/vigou3/methodes-numeriques-en-actuariat

## Nom de la partie
PARTNAME = algebre-lineaire

## Nom du fichier maître
MASTER = methodes-numeriques-en-actuariat_${PARTNAME}.pdf

## Le document maitre dépend de: tous les fichiers .Rnw; tous les
## fichiers .tex autres que lui-même qui n'ont pas de version .Rnw;
## tous les fichiers .R qui ont un fichier .Rnw ou .tex correspondant;
## les fichiers partagés .tex du répertoire 'share'.
RNWFILES = $(wildcard *.Rnw)
TEXFILES = $(addsuffix .tex,\
                       $(filter-out $(basename ${RNWFILES} ${MASTER} $(wildcard solutions-*.tex)),\
                                    $(basename $(wildcard *.tex))))	
SCRIPTS = $(addsuffix .R,\
                      $(filter $(basename $(wildcard *.R)),\
                               $(basename ${RNWFILES} ${TEXFILES})))
SHARED = $(wildcard ../share/*.tex)

## Outils de travail
SWEAVE = R CMD Sweave --encoding="utf-8"
TEXI2DVI = LATEX=xelatex texi2dvi -b
RBATCH = R CMD BATCH --no-timing
CP = cp -p
RM = rm -rf

all: pdf

%.tex: %.Rnw
	${SWEAVE} '$<'

%.Rout: %.R
	echo "options(error=expression(NULL))" | cat - $< | \
	  sed -e 's/`.*`//' \
	      -e 's/ *#-\*-.*//' \
	  > $<.tmp
	${RBATCH} $<.tmp $@
	${RM} $<.tmp

${MASTER}: ${MASTER:.pdf=.tex} ${RNWFILES:.Rnw=.tex} ${TEXFILES} ${SCRIPTS} ${SHARED}
	${TEXI2DVI} ${MASTER:.pdf=.tex}

.PHONY: pdf
pdf: ${MASTER}

.PHONY: tex
tex: ${RNWFILES:.Rnw=.tex}

.PHONY: Rout
Rout: ${SCRIPTS:.R=.Rout}

## la règle 'install' ne devrait être appelée que par ../Makefile
.PHONY: install
install: pdf Rout
	for f in ${SCRIPTS}; \
	    do sed -e 's/`.*`//' \
	           -e 's/ *#-\*-.*//' \
	           $$f > ../${BUILDDIR}/${PARTNAME}/$$f; \
	done
	${CP} ${MASTER} ${SCRIPTS:.R=.Rout} ../${BUILDDIR}/${PARTNAME}

.PHONY: check-url
check-url: ${MASTER:.pdf=.tex} ${RNWFILES} ${TEXFILES} ${SCRIPTS} ${SHARED}
	@echo ----- Checking urls in sources...
	$(eval url=$(shell grep -E -o -h 'https?:\/\/[^./]+(?:\.[^./]+)+(?:\/[^ ]*)?' $? \
		   | cut -d \} -f 1 \
		   | cut -d ] -f 1 \
		   | cut -d '"' -f 1 \
		   | sort | uniq))
	for u in ${url}; \
	    do if curl --output /dev/null --silent --head --fail --max-time 5 $$u; then \
	        echo "URL exists: $$u"; \
	    else \
		echo "URL does not exist (or times out): $$u"; \
	    fi; \
	done

.PHONY: clean
clean:
	${RM} ${MASTER} \
	      ${RNWFILES:.Rnw=.tex} \
	      ${SCRIPTS:.R=.Rout} \
	      solutions-* \
	      *-[0-9][0-9][0-9].pdf \
	      *.aux *.log  *.blg *.bbl *.out *.rel *~ Rplots* .RData
