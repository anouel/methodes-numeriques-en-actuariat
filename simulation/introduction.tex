%%% Copyright (C) 2018 Vincent Goulet
%%%
%%% Ce fichier fait partie du projet
%%% «Méthodes numériques en actuariat avec R»
%%% https://gitlab.com/vigou3/methodes-numeriques-en-actuariat
%%%
%%% Cette création est mise à disposition sous licence
%%% Attribution-Partage dans les mêmes conditions 4.0
%%% International de Creative Commons.
%%% https://creativecommons.org/licenses/by-sa/4.0/

\chapter*{Introduction}
\addcontentsline{toc}{chapter}{Introduction}
\markboth{Introduction}{Introduction}

La simulation stochastique est une technique utilisée dans un grand
nombre de domaines. Pensons seulement aux simulations boursières qui
font l'objet d'un concours annuel, aux voitures qui sont d'abord
conçues sur ordinateur et soumises à des tests de collisions virtuels,
ou encore aux prévisions météo qui ne sont en fait que les résultats de
simulations de systèmes climatiques d'une grande complexité.

Toute simulation stochastique repose sur une source de nombres
aléatoires de qualité. Comment en générer un grand nombre rapidement
et, surtout, comment s'assurer que les nombres produits sont bien
aléatoires? C'est un sujet d'une grande importance, mais aussi fort
complexe. Nous nous contenterons donc de l'effleurer en étudiant les
techniques de base dans le \autoref{chap:generation}.

En actuariat, nous avons habituellement besoin de nombres aléatoires
provenant d'une loi de probabilité non uniforme. Le
\autoref{chap:simulation} présente quelques algorithmes pour
transformer des nombres aléatoires uniformes en nombres non uniformes.
Évidemment, des outils informatiques sont aujourd'hui disponibles pour
générer facilement et rapidement des nombres aléatoires de diverses
lois de probabilité. Nous passons en revue les fonctionnalités de R et
d'Excel à cet égard.

Enfin, l'ouvrage se termine au \autoref{chap:montecarlo} par une
application à première vue inusitée de la simulation, soit le calcul
d'intégrales définies par la méthode dite «Monte~Carlo».

\section*{Utilisation de l'ouvrage}

Chaque chapitre de cet ouvrage propose un problème à résoudre au fil
du texte. L'énoncé du problème, les indices en cours de chapitre et la
solution complète se présentent dans des sections marquées des
symboles {\enoncesign}, {\astucesign} et {\solutionsign}.

L'étude de l'ouvrage implique des allers-retours entre le texte et le
code R à la fin de chaque chapitre. Ce code informatique et les
commentaires qui l'accompagnent visent à enrichir vos apprentissages.
Assurez-vous donc de lire attentivement tant les commentaires que
le code, d'exécuter le code pas-à-pas et de bien comprendre ses
effets.

Le code informatique est distribué avec l'ouvrage sous forme de
fichiers de script. De plus, à chaque fichier \code{.R} correspond un
fichier \code{.Rout} contenant les résultats de son évaluation non
interactive.

Vous trouverez également en annexe un bref exposé sur la planification
d'une simulation en R, des rappels sur la transformation de variables
aléatoires, de même que les solutions complètes des exercices.

\section*{Fonctionnalités interactives}

En consultation électronique, ce document se trouve enrichi de
plusieurs fonctionnalités interactives.
\begin{itemize}
\item Intraliens du texte vers une ligne précise d'une section de code
  informatique et, en sens inverse, du numéro de la ligne vers le
  point de la référence dans le texte. Ces intraliens sont marqués par
  la couleur \textcolor{link}{\rule{1.5em}{1.2ex}}.
\item Intraliens entre le numéro d'un exercice et sa solution, et vice
  versa. Ces intraliens sont aussi marqués par la couleur
  \textcolor{link}{\rule{1.5em}{1.2ex}}.
\item Intraliens entre les citations dans le texte et leur entrée dans
  la bibliographie. Ces intraliens sont marqués par la couleur
  \textcolor{citation}{\rule{1.5em}{1.2ex}}.
\item Hyperliens vers des ressources externes marqués par le symbole
  {\smaller\faExternalLink} et la couleur
  \textcolor{url}{\rule{1.5em}{1.2ex}}.
\item Table des matières, liste des tableaux, liste des figures et
  liste des vidéos permettant d'accéder rapidement à des ressources du
  document.
\end{itemize}

\section*{Blocs signalétiques}

Le document est parsemé de divers types de blocs signalétiques
inspirés de
\link{https://asciidoctor.org/docs/user-manual/\#admonition}{AsciiDoc}
qui visent à attirer votre attention sur une notion. Vous pourrez
rencontrer l'un ou l'autre des blocs suivants.

\tipbox{Astuce! Ces blocs contiennent un truc, une astuce, ou tout
  autre type d'information utile.}
\vspace{-\baselineskip}

\warningbox{Avertissement! Ces blocs mettent l'accent sur une notion
  ou fournissent une information importante pour la suite.}
\vspace{-\baselineskip}

\cautionbox{Attention! Vous risquez de vous brûler ---
  métaphoriquement s'entend --- si vous ne suivez pas les
  recommandations de ces blocs.}
\vspace{-\baselineskip}

\importantbox{Important! Ces blocs contiennent les remarques les plus
  importantes. Veillez à en tenir compte.}
\vspace{-\baselineskip}

\notebox{Ces blocs contiennent des remarques additionnelles sur
  la matière ou des informations amusantes, mais non essentielles.}
\vspace{-\baselineskip}

%% Utilisation de la commande \awesomebox pour illustrer les blocs de
%% vidéos pour éviter de créer une entrée dans la liste des vidéos.
\awesomebox{\faYoutubePlay}{\aweboxrulewidth}{url}{Ces blocs
  contiennent des liens vers des vidéos dans ma %
  \link{https://www.youtube.com/channel/UCRNwk6mEdUc0njqytxXBDOw/videos}{chaine
    YouTube} dédiée à ce document de référence. Les vidéos sont
  répertoriées dans la liste des vidéos.}
\vspace{-\baselineskip}

\gotorbox{Ces blocs vous invitent à interrompre la lecture du texte
  pour passer à l'étude du code R des sections d'exemples.}
\vspace{-\baselineskip}

\osxbox{Remarques spécifiques à macOS.}

\section*{Document libre}

Tout comme R et l'ensemble des outils présentés dans ce document, le
projet «Méthodes numériques en actuariat avec R» s'inscrit dans le
mouvement de
l'\link{https://www.gnu.org/philosophy/free-sw.html}{informatique
  libre}. Vous pouvez accéder à l'ensemble du code source en format
{\LaTeX} en suivant le lien dans la page de copyright. Vous trouverez
dans le fichier \code{README.md} toutes les informations utiles pour
composer le document.

Votre contribution à l'amélioration du document est également la
bienvenue; consultez le fichier \code{CONTRIBUTING.md} fourni avec ce
document et voyez votre nom ajouté au fichier \code{COLLABORATEURS}.

\section*{Remerciements}

Je tiens à souligner la précieuse collaboration de MM.~Mathieu
Boudreault, Sébastien Auclair et Louis-Philippe Pouliot lors de la
rédaction des exercices et des solutions.

%%% Local Variables:
%%% TeX-master: "methodes-numeriques-en-actuariat_simulation"
%%% TeX-engine: xetex
%%% coding: utf-8
%%% End:
