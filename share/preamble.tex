%%% Copyright (C) 2018 Vincent Goulet
%%%
%%% Ce fichier fait partie du projet
%%% «Méthodes numériques en actuariat avec R»
%%% https://gitlab.com/vigou3/methodes-numeriques-en-actuariat
%%%
%%% Cette création est mise à disposition sous licence
%%% Attribution-Partage dans les mêmes conditions 4.0
%%% International de Creative Commons.
%%% https://creativecommons.org/licenses/by-sa/4.0/

\documentclass[letterpaper,11pt,x11names,english,french]{memoir}
  \usepackage{natbib,url}
  \usepackage{babel}
  \usepackage[autolanguage]{numprint}
  \usepackage{amsmath,amsthm}
  \usepackage[noae]{Sweave}
  \usepackage{graphicx}
  \usepackage{currfile}                % pour noms fichiers de script
  \usepackage{actuarialangle}          % \angl et al.
  \usepackage{framed}                  % env. snugshade*, oframed
  \usepackage{paralist}                % listes au fil du texte
  \usepackage[absolute]{textpos}       % éléments des pages de titre
  \usepackage[shortlabels]{enumitem}   % configuration listes
  \usepackage{relsize}                 % \smaller et al.
  \usepackage{manfnt}                  % \mantriangleright (puce)
  \usepackage{metalogo}                % \XeLaTeX logo
  \usepackage{fontawesome}             % icônes \fa*
  \usepackage{awesomebox}              % boites info, important, etc.
  \usepackage{answers}                 % exercices et solutions
  \usepackage{listings}                % code informatique
  \usepackage{refcount}                % numéros de ligne en surbrillance
  \usepackage{xr}                      % références entre parties

  %%% =======================================================
  %%%  Informations de publication (sauf titre de la partie)
  %%% =======================================================
  \title{Méthodes numériques en actuariat avec R}
  \author{Vincent Goulet}
  \renewcommand{\year}{2019}
  \renewcommand{\month}{05}
  \newcommand{\reposurl}{https://gitlab.com/vigou3/methodes-numeriques-en-actuariat/}

  %%% ===================
  %%%  Style du document
  %%% ===================

  %% Polices de caractères
  \usepackage{fontspec}
  \usepackage{unicode-math}
  \defaultfontfeatures
  {
    Scale = 0.92
  }
  \setmainfont{Lucida Bright OT}
  [
    Ligatures = TeX,
    Numbers = OldStyle
  ]
  \setmathfont{Lucida Bright Math OT}
  \setmonofont{Lucida Grande Mono DK}
  \setsansfont{FiraSans}
  [
    Extension = .otf,
    UprightFont = *-Book,
    BoldFont = *-SemiBold,
    BoldItalicFont = *-SemiBoldItalic,
    Scale = 0.98,
    Numbers = OldStyle
  ]
  \newfontfamily\fullcaps{FiraSans}
  [
    Extension = .otf,
    UprightFont = *-Book,
    Scale = 0.98,
    Numbers = Uppercase
  ]
  \usepackage[babel=true]{microtype}
  \usepackage{icomma}

  %% Couleurs
  \usepackage{xcolor}
  \definecolor{comments}{rgb}{0.7,0,0}           % commentaires
  \definecolor{link}{rgb}{0,0.4,0.6}             % liens internes
  \definecolor{url}{rgb}{0.6,0,0}                % liens externes
  \definecolor{citation}{rgb}{0,0.5,0}           % citations
  \definecolor{codebg}{named}{LightYellow1}      % fond code R
  \definecolor{prob}{named}{orange}              % encadrés «problème»
  \definecolor{lineno}{named}{gray}              % numéros de lignes
  \definecolor{rouge}{rgb}{0.85,0,0.07} % rouge bandeau identitaire
  \definecolor{or}{rgb}{1,0.8,0}        % or bandeau identitaire

  %% Hyperliens
  \usepackage{hyperref}
  \hypersetup{%
    pdfauthor = {Vincent Goulet},
    colorlinks = {true},
    linktocpage = {true},
    urlcolor = {url},
    linkcolor = {link},
    citecolor = {citation},
    pdfpagemode = {UseOutlines},
    pdfstartview = {Fit},
    bookmarksopen = {true},
    bookmarksnumbered = {true},
    bookmarksdepth = {subsubsection}}
  \setlength{\XeTeXLinkMargin}{1pt}

  %% Étiquettes de \autoref (redéfinitions compatibles avec babel).
  %% Attention! Les % à la fin des lignes sont importants sinon des
  %% blancs apparaissent dès que la commande \selectlanguage est
  %% utilisée... comme dans la bibliographie, par exemple.
  \addto\extrasfrench{%
    \def\algorithmeautorefname{algorithme}%
    \def\appendixautorefname{annexe}%
    \def\definitionautorefname{définition}%
    \def\figureautorefname{figure}%
    \def\exempleautorefname{exemple}%
    \def\exerciceautorefname{exercice}%
    \def\subfigureautorefname{figure}%
    \def\subsectionautorefname{section}%
    \def\subtableautorefname{tableau}%
    \def\tableautorefname{tableau}%
    \def\thmautorefname{théorème}%
  }

  %% Table des matières (inspirée de classicthesis.sty)
  \renewcommand{\cftchapterleader}{\hspace{1.5em}}
  \renewcommand{\cftchapterafterpnum}{\cftparfillskip}
  \renewcommand{\cftsectionleader}{\hspace{1.5em}}
  \renewcommand{\cftsectionafterpnum}{\cftparfillskip}
  \renewcommand{\cfttableleader}{\hspace{1.5em}}
  \renewcommand{\cfttableafterpnum}{\cftparfillskip}
  \renewcommand{\cftfigureleader}{\hspace{1.5em}}
  \renewcommand{\cftfigureafterpnum}{\cftparfillskip}
  \setlength{\cftfigurenumwidth}{3.2em}

  %% Création d'une «liste des vidéos» similaires à la
  %% liste des tableaux et à la liste des figures.
  \newcommand{\listvideosname}{Liste des vidéos}
  \newlistof{listofvideos}{lov}{\listvideosname}
  \renewcommand{\afterlovtitle}{%
    \afterchaptertitle{\normalfont\normalsize Le numéro indiqué à
      gauche est celui de la section dans laquelle se trouve le bloc
      signalétique.\label{listofvideos}}\vspace{10pt}}

  %% Titres des chapitres
  \chapterstyle{hangnum}
  \renewcommand{\chaptitlefont}{\normalfont\Huge\sffamily\bfseries\raggedright}

  %% Marges, entêtes et pieds de page
  \setlength{\marginparsep}{7mm}
  \setlength{\marginparwidth}{13mm}
  \marginparmargin{left}
  \setlength{\headwidth}{\textwidth}
  \addtolength{\headwidth}{\marginparsep}
  \addtolength{\headwidth}{\marginparwidth}

  %% Titres des sections et sous-sections
  \setsecheadstyle{\normalfont\Large\sffamily\bfseries\raggedright}
  \setsubsecheadstyle{\normalfont\large\sffamily\bfseries\raggedright}
  \maxsecnumdepth{subsection}
  \setsecnumdepth{subsection}

  %% Listes. Paramétrage avec enumitem.
  \setlist[enumerate]{leftmargin=*,align=left}
  \setlist[enumerate,2]{label=\alph*),labelsep=*,leftmargin=1.5em}
  \setlist[enumerate,3]{label=\roman*),labelsep=*,leftmargin=1.5em,align=right}
  \setlist[itemize]{leftmargin=*,align=left}

  %% Options de babel
  \frenchbsetup{StandardItemizeEnv=true,%
    ThinSpaceInFrenchNumbers=true,
    ItemLabeli=\mantriangleright,
    ItemLabelii=\textendash,
    og=«, fg=»}
  \addto\captionsfrench{\def\figurename{{\scshape Fig.}}}
  \addto\captionsfrench{\def\tablename{{\scshape Tab.}}}
  \addto\captionsfrench{\def\listfigurename{Liste des figures}}

  %%% =========================
  %%%  Sections de code source
  %%% =========================

  %% Syntaxe de R
  \lstloadlanguages{R}

  %% Mise en forme du code source.
  %%
  %% Les numéros de lignes sont des hyperliens vers le point dans le
  %% document où l'on y fait référence dans une boite \gotorbox (voir
  %% plus loin).
  %%
  %% Pour y parvenir, j'utilise deux étiquettes pour une ligne: une
  %% basée sur un "nom" utilisé dans la rédaction, et une autre
  %% générée automatiquement à partir du numéro de chapitre et du
  %% numéro de ligne.
  %%
  %% Solution basée sur https://tex.stackexchange.com/q/191771
  \lstset{%
    language=R,
    extendedchars=true,
    basicstyle=\small\ttfamily\NoAutoSpacing,
    commentstyle=\color{comments}\slshape,
    keywordstyle=\mdseries,
    showstringspaces=false,
    numbers=left,
    numberstyle={%
      \color{lineno}\tiny\ttfamily%
      \ifnum\value{lstnumber}=\getrefnumber{code:\thechapter:\thelstnumber}%
        \renewcommand*\thelstnumber{\hyperlink{goto:\thechapter:\the\value{lstnumber}}{\bfseries\arabic{lstnumber}}}%
      \fi},
    firstnumber=\scriptfirstline,
    escapechar=`,
    moredelim=[il]{\#-*-}}

  %% Commandes pour créer les références et liens vers des numéros de
  %% lignes.
  \makeatletter
  \newcommand\labelline[1]{%
    \def\@currentlabel{\thelstnumber}%
    \label{lst:#1}\label{code:\thechapter:\the\value{lstnumber}}}
  \makeatother
  \newcommand{\reflines}[1]{%
    \hypertarget{goto:\thechapter:\getrefnumber{lst:#1}}{\ref{lst:#1}}--%
    \hypertarget{goto:\thechapter:\getrefnumber{lst:#1:fin}}{\ref{lst:#1:fin}}}

  %% L'entête des fichiers de script n'est pas affiché dans le
  %% document.
  \def\scriptfirstline{14}      % nombre magique!

  %%% =========================
  %%%  Nouveaux environnements
  %%% =========================

  %% Environnements d'exemples et al.
  \theoremstyle{plain}
  \newtheorem{algorithme}{Algorithme}[chapter]
  \newtheorem{thm}{Théorème}[chapter]
  \newtheorem{lemme}{Lemme}[chapter]

  \theoremstyle{definition}
  \newtheorem{exemple}{Exemple}[chapter]
  \newtheorem{definition}{Définition}[chapter]
  \newtheorem*{astuce}{Astuce}

  %% Redéfinition de l'environnement titled-frame de framed.sty avec
  %% deux modifications: épaisseur des filets réduite de 2pt à 1pt;
  %% "(suite)" plutôt que "(cont)" dans la barre de titre
  %% lorsque l'encadré se poursuit après un saut de page.
  \renewenvironment{titled-frame}[1]{%
    \def\FrameCommand{\fboxsep8pt\fboxrule1pt
      \TitleBarFrame{\textbf{#1}}}%
    \def\FirstFrameCommand{\fboxsep8pt\fboxrule1pt
      \TitleBarFrame[$\blacktriangleright$]{\textbf{#1}}}%
    \def\MidFrameCommand{\fboxsep8pt\fboxrule1pt
      \TitleBarFrame[$\blacktriangleright$]{\textbf{#1\ (suite)}}}%
    \def\LastFrameCommand{\fboxsep8pt\fboxrule1pt
      \TitleBarFrame{\textbf{#1\ (suite)}}}%
    \MakeFramed{\advance\hsize-16pt \FrameRestore}}%
  {\endMakeFramed}

  %% Liste d'objectifs au début des chapitres dans un encadré
  %% basé sur titled-frame, ci-dessus.
  \newenvironment{objectifs}{%
    \colorlet{TFFrameColor}{black}%
    \colorlet{TFTitleColor}{white}%
    \begin{titled-frame}{\rule[-7pt]{0pt}{20pt}\sffamily Objectifs du chapitre}
      \setlength{\parindent}{0pt}
      \small\sffamily
      \begin{itemize}[nosep]}%
      {\end{itemize}\end{titled-frame}}

  %% Problèmes (mises en situation) des chapitres: énoncé au début du
  %% chapitre; astuces en cours de chapitre; solution à la fin
  %% du chapitre. [Janvier 2019: ce ne sont plus véritablement des
  %% environnements, mais conservés comme tels pour éviter de devoir
  %% retravailler le texte.]
  \def\enoncesign{\faCogs}      % engrenages
  \def\astucesign{\faLifeRing}  % bouée
  \def\solutionsign{\faBolt}    % éclair
  \newenvironment{prob-enonce}{%
    \section[Énoncé du problème]{Énoncé du problème \marginpar{\normalfont\Large\hfill\enoncesign}}}{}
  \newenvironment{prob-astuce}{%
    \section[Indice pour le problème]{Indice pour le problème \marginpar{\normalfont\Large\hfill\astucesign}}}{}
  \newenvironment{prob-solution}{%
    \section[Solution du problème]{Solution du problème \marginpar{\normalfont\Large\hfill\solutionsign}}}{}

  %% Environnements de Sweave. Les environnements Sinput et Soutput
  %% utilisent Verbatim (de fancyvrb). On les réinitialise pour
  %% enlever la configuration par défaut de Sweave, puis on réduit
  %% l'écart entre les blocs Sinput et Soutput.
  \DefineVerbatimEnvironment{Sinput}{Verbatim}{}
  \DefineVerbatimEnvironment{Soutput}{Verbatim}{}
  \fvset{listparameters={\setlength{\topsep}{0pt}}}

  %% L'environnement Schunk est complètement redéfini en un hybride
  %% des environnements snugshade* et leftbar de framed.sty.
  \makeatletter
  \renewenvironment{Schunk}{%
    \setlength{\topsep}{1pt}
    \def\FrameCommand##1{\hskip\@totalleftmargin
       \vrule width 2pt\colorbox{codebg}{\hspace{3pt}##1}%
      % There is no \@totalrightmargin, so:
      \hskip-\linewidth \hskip-\@totalleftmargin \hskip\columnwidth}%
    \MakeFramed {\advance\hsize-\width
      \@totalleftmargin\z@ \linewidth\hsize
      \advance\labelsep\fboxsep
      \@setminipage}%
  }{\par\unskip\@minipagefalse\endMakeFramed}
  \makeatother

  %% Exercices et réponses
  \Newassociation{sol}{solution}{solutions}
  \Newassociation{rep}{reponse}{reponses}
  \newcounter{exercice}[chapter]
  \renewcommand{\theexercice}{\thechapter.\arabic{exercice}}
  \newenvironment{exercice}[1][]{%
    \begin{list}{}{%
        \refstepcounter{exercice}
        \ifthenelse{\equal{#1}{nosol}}{%
          \renewcommand{\makelabel}{\bfseries\theexercice}}{%
          \hypertarget{ex:\theexercice}{}
          \Writetofile{solutions}{\protect\hypertarget{sol:\theexercice}{}}
          \renewcommand{\makelabel}{%
            \bfseries\protect\hyperlink{sol:\theexercice}{\theexercice}}}
        \settowidth{\labelwidth}{\bfseries\theexercice}
        \setlength{\leftmargin}{\labelwidth}
        \addtolength{\leftmargin}{\labelsep}
        \setlist[enumerate,1]{label=\alph*),labelsep=*,leftmargin=1.5em}
        \setlist[enumerate,2]{label=\roman*),labelsep=0.5em,align=right}}
      \item}%
      {\end{list}}
  \renewenvironment{solution}[1]{%
    \begin{list}{}{%
        \renewcommand{\makelabel}{%
          \bfseries\protect\hyperlink{ex:#1}{#1}}
        \settowidth{\labelwidth}{\bfseries #1}
        \setlength{\leftmargin}{\labelwidth}
        \addtolength{\leftmargin}{\labelsep}
        \setlist[enumerate,1]{label=\alph*),labelsep=*,leftmargin=1.5em}
        \setlist[enumerate,2]{label=\roman*),labelsep=0.5em,align=right}}
    \item}%
    {\end{list}}
  \renewenvironment{reponse}[1]{%
    \begin{enumerate}[label=\textbf{#1}]
    \item}%
    {\end{enumerate}}

  %% Redéfinition de l'environnement de matrices de amsmath pour
  %% aligner les colonnes à droite. Pris dans
  %% http://texblog.net/latex-archive/maths/matrix-align-left-right
  \makeatletter
  \renewcommand*\env@matrix[1][r]{\hskip -\arraycolsep
    \let\@ifnextchar\new@ifnextchar
    \array{*\c@MaxMatrixCols #1}}
  \makeatother

  %%% =====================
  %%%  Nouvelles commandes
  %%% =====================

  %% Noms de fonctions, code, etc.
  \newcommand{\code}[1]{\texttt{#1}}
  \newcommand{\pkg}[1]{\textbf{#1}}

  %% Hyperlien avec symbole de lien externe juste après; second
  %% argument peut être vide pour afficher l'url comme lien
  %% [https://tex.stackexchange.com/q/53068/24355 pour procédure de
  %% test du second paramètre vide]
  \newcommand{\link}[2]{%
    \def\param{#2}%
    \ifx\param\empty
      \href{#1}{\nolinkurl{#1}~\raisebox{-0.1ex}{\smaller\faExternalLink}}%
    \else
      \href{#1}{#2~\raisebox{-0.1ex}{\smaller\faExternalLink}}%
    \fi
  }

  %% Boite additionnelle (basée sur awesomebox.sty) pour liens vers
  %% des vidéos et ajout à la liste des vidéos.
  \newcounter{videos}[chapter]
  \newcommand{\videobox}[2]{%
    \refstepcounter{videos}
    \addcontentsline{lov}{figure}{\protect\numberline{\thesection}\protect#1}
    \awesomebox{\faYoutubePlay}{\aweboxrulewidth}{url}{#2}}

  %% Boites additionnelles (basées sur awesomebox.sty) pour remarques
  %% spécifiques à macOS et pour les changements au fil de la lecture.
  \newcommand{\osxbox}[1]{%
    \awesomebox{\faApple}{\aweboxrulewidth}{black}{#1}}
  \newcommand{\gotorbox}[1]{%
    \awesomebox{\faMapSigns}{\aweboxrulewidth}{black}{#1}}

  %% Boite pour le nom du fichier de script correspondant au début des
  %% sections d'exemples.
  \newcommand{\scriptfile}[1]{%
    \begingroup
    \noindent
    \mbox{%
      \makebox[3mm][l]{\raisebox{-0.5pt}{\small\faChevronCircleDown}}\;%
      \smaller[1] {\sffamily Fichier d'accompagnement} {\ttfamily #1}}
    \endgroup}

  %% Lien vers GitLab dans la page de notices
  \newcommand{\viewsource}[1]{%
    \href{#1}{\faGitlab\ Voir sur GitLab}}

  %% Raccourcis usuels vg
  \newcommand{\pt}{{\scriptscriptstyle \Sigma}}
  \newcommand{\abs}[1]{\lvert #1 \rvert}
  \newcommand{\norm}[1]{\lVert #1 \rVert}
  \newcommand{\mat}[1]{\symbf{#1}}
  \newcommand{\diag}{\operatorname{diag}}
  \newcommand{\Esp}[1]{E\! \left[ #1 \right]}
  \newcommand{\esp}[1]{E [ #1 ]}
  \newcommand{\Var}[1]{\operatorname{Var}\! \left[ #1 \right]}
  \newcommand{\var}[1]{\operatorname{Var} [ #1 ]}
  \newcommand{\Prob}[1]{\operatorname{Pr}\! \left[ #1 \right]}
  \newcommand{\prob}[1]{\operatorname{Pr} [ #1 ]}
  \newcommand{\R}{\symbb{R}}    % ensemble des réels

  %% Traitement du titre de partie
  \makeatletter
  \newcommand{\@parttitle}{}
  \newcommand{\parttitle}[1]{\renewcommand{\@parttitle}{#1}}
  \newcommand{\theparttitle}{\@parttitle}
  \makeatother

  %%% =======
  %%%  Varia
  %%% =======

  %% Sous-tableaux et figures
  \newsubfloat{table}
  \newsubfloat{figure}

  %% Style de la bibliographie
  \bibliographystyle{francais}

  %% Longueurs pour la composition des pages couvertures avant et
  %% arrière.
  \newlength{\banderougewidth} \newlength{\banderougeheight}
  \newlength{\bandeorwidth}    \newlength{\bandeorheight}
  \newlength{\imageheight}
  \newlength{\logoheight}

  %% Aide pour la césure
  \hyphenation{%
    con-gru-en-tiels
    con-naî-tre
    con-sole
    cons-tante
    con-tenu
    con-trôle
    hexa-dé-ci-mal
    nom-bre
    puis-que
  }
